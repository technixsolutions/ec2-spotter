module EC2Spotter
  module Job
    
    class DescribeVolume
      include Backburner::Queue
      queue "describe-volume"
      queue_priority 50
      queue_respond_timeout 300
    
      def self.perform(volume_id)
        if volume = Snapshot[volume_id]
          volume.describe
        else
          puts "Failed to find Volume ##{volume_id}!"
        end
      end
      
    end
  end
end