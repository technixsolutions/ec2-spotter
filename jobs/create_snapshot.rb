module EC2Spotter
  module Job
    class CreateSnapshot
      include Backburner::Queue
      queue "create-snapshot"
      queue_priority 250
      queue_respond_timeout 300
    
      def self.perform(volume_id)
        if volume = Volume[volume_id]
          volume.create_snapshot(true)
        else
          puts "Failed to find Volume ##{volume_id}!"
        end
      end
      
    end
  end
end