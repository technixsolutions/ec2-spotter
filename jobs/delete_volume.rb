module EC2Spotter
  module Job
    class DeleteVolume
      include Backburner::Queue
      
      queue "delete-volume"
      queue_priority 250
      queue_respond_timeout 300
    
      def self.perform(volume_id)
        if volume = Volume[volume_id]
          volume.delete_volume(true)
        else
          puts "Failed to find Volume ##{volume_id}!"
        end
      end
      
    end
  end
end