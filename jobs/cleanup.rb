module EC2Spotter
  module Job
    class Cleanup
      include Backburner::Queue
      
      queue "cleanup"
      queue_priority 500
      queue_respond_timeout 300
    
      def self.perform(spot_request_id)
        if spot_request = SpotRequest[spot_request_id]
          spot_request.cleanup
        else
          puts "Could not find Spot Request ##{spot_request_id}!"
        end
      end
      
    end
  end
end