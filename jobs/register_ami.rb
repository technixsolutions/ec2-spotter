module EC2Spotter
  module Job
    class RegisterAMI
      include Backburner::Queue
      
      queue "register-ami"
      queue_priority 250
      queue_respond_timeout 300
    
      def self.perform(instance_id)
        if instance = Instance[instance_id]
          instance.register_image(true)
        else
          puts "Failed to find Instance ##{instance_id}!"
        end
      end
      
    end
  end
end