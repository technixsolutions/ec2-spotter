module EC2Spotter
  module Job
    
    class DescribeSnapshot
      include Backburner::Queue
      queue "describe-snapshot"
      queue_priority 50
      queue_respond_timeout 300
    
      def self.perform(snapshot_id)
        if snapshot = Snapshot[snapshot_id]
          snapshot.describe
        else
          puts "Failed to find Snapshot ##{snapshot_id}!"
        end
      end
      
    end
  end
end