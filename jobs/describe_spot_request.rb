module EC2Spotter
  module Job
    
    class DescribeSpotRequest
      include Backburner::Queue
      queue "describe-spot-request"
      queue_priority 50
      queue_respond_timeout 300
    
      def self.perform(spot_request_id)
        if spot_request = SpotRequest[spot_request_id]
          spot_request.describe
        else
          puts "Failed to find Spot Request ##{spot_request_id}!"
        end
      end
      
    end
  end
end