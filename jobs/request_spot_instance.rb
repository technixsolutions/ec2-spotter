module EC2Spotter
  module Job
    class RequestSpotInstance
      include Backburner::Queue
      
      queue "request-spot-instance"
      queue_priority 100
      queue_respond_timeout 300
    
      def self.perform(instance_id)
        if instance = Instance[instance_id]
          instance.request_spot_instance(true)
        else
          puts "Failed to find Instance ##{instance_id}!"
        end
      end
      
    end
  end
end