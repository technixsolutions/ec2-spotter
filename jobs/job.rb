module EC2Spotter
  module Job
    
    class Base
      include Backburner::Queue
      
      class << self
        def self.perform(&block)
          return_value = block.call
          if return_value == :bury
            self.bury
          end
        end
      end
    end
    
  end
end