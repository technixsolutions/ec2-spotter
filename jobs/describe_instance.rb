module EC2Spotter
  module Job
    
    class DescribeInstance
      include Backburner::Queue
      queue "describe-instance"
      queue_priority 50
      queue_respond_timeout 300
    
      def self.perform(instance_id)
        if instance = Instance[instance_id]
          instance.describe
        else
          puts "Could not find Instance ##{instance_id}!"
        end
      end
      
    end
  end
end