module EC2Spotter
  module Job
    class DeleteSnapshot
      include Backburner::Queue
      
      queue "delete-snapshot"
      queue_priority 250
      queue_respond_timeout 300
    
      def self.perform(snapshot_id)
        if snapshot = Snapshot[snapshot_id]
          snapshot.delete_snapshot(true)
        else
          puts "Failed to find Snapshot ##{snapshot_id}!"
        end
      end
      
    end
  end
end