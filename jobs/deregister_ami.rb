module EC2Spotter
  module Job
    class DeregisterAMI
      include Backburner::Queue
      
      queue "deregister-ami"
      queue_priority 250
      queue_respond_timeout 300
    
      def self.perform(image_id)
        if image = Image[image_id]
          image.deregister(true)
        else
          puts "Failed to find Image ##{image_id}!"
        end
      end
      
    end
  end
end