module EC2Spotter
  module Job
    
    class DescribeImage
      include Backburner::Queue
      queue "describe-image"
      queue_priority 50
      queue_respond_timeout 300
    
      def self.perform(image_id)
        if image = Snapshot[image_id]
          image.describe
        else
          puts "Failed to find Image ##{image_id}!"
        end
      end
      
    end
  end
end