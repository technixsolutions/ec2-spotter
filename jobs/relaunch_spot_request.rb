module EC2Spotter
  module Job
    
    class RelaunchSpotRequest
      include Backburner::Queue
      queue "relaunch-spot-request"
      queue_priority 50
      queue_respond_timeout 300
    
      def self.perform(spot_request_id)
        if spot_request = SpotRequest[spot_request_id]
          if instance = spot_request.instance
            if volume = instance.volume
              Backburner.enqueue Job::CreateSnapshot, volume.id
            else
              puts "Instance ##{instance.id} is not associated with a Volume!"
            end
          else
            puts "Spot Request ##{spot_request_id} is not associated with an Instance!"
          end
        else
          puts "Failed to find Spot Request ##{spot_request_id}!"
        end
      end
      
    end
  end
end