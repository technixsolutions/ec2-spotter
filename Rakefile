require './app'

namespace :monitor do
  task :instances do
    EC2Spotter::Monitor.run
  end
  
  task :spot_prices do
    EC2Spotter::SpotPrice.describe(['us-east-1a', 'us-east-1b', 'us-east-1c', 'us-east-1d'], ['m1.micro', 'm1.small', 'm1.medium', 'm1.large', 'm1.xlarge'])
  end
end

namespace :worker do
  task :run do
    Backburner.work
  end
end

namespace :db do
  task :create do
    DB.create_table :spot_prices do
      primary_key :id
      DateTime :created_at
      DateTime :updated_at
      
      String :availability_zone
      String :instance_type
      DateTime :aws_timestamp
      Float :price
    end
    
    DB.create_table :spot_requests do
      primary_key :id
      DateTime :created_at
      DateTime :updated_at
      
      foreign_key :instance_id
      String :aws_id, :unique => true
      Boolean :describing, :default => false
      Boolean :described, :default => false
      Boolean :available
      Boolean :cancelled
      String :status
      
      Float :price
      
    end
    
    DB.create_table :instances do
      primary_key :id
      DateTime :created_at
      DateTime :updated_at
      
      foreign_key :image_id
      foreign_key :volume_id
      #String :old_volume_id
      #String :old_snapshot_id
      #String :old_image_id
      
      String :aws_id, :unique => true
      Boolean :describing, :default => false
      Boolean :described, :default => false
      Boolean :available
      Boolean :terminated
      Boolean :relaunched
      String :status
      Integer :relaunches, :default => 0
      Integer :relaunch_state, :size => :tiny
      String :spot_request_id
      
      DateTime :launch_time
      String :instance_type
      String :availability_zone
      String :key_name
      String :root_device
      String :architecture
      String :kernel_id
      String :public_ip
      String :public_dns
    end
    
    DB.create_table :images do
      primary_key :id
      DateTime :created_at
      DateTime :updated_at
      
      foreign_key :snapshot_id
      String :aws_id, :unique => true
      Boolean :deregistered, :default => false
      Boolean :describing, :default => false
      Boolean :described, :default => false
      Boolean :available
      String :status
      
      String :root_device
      String :volume_size
      String :architecture
      String :kernel_id
      String :description
      
    end
    
    DB.create_table :volumes do
      primary_key :id
      DateTime :created_at
      DateTime :updated_at
      
      String :aws_id, :unique => true
      Boolean :deleted, :default => false
      Boolean :describing, :default => false
      Boolean :described, :default => false
      Boolean :available
      String :status
      Boolean :attaching
      Boolean :attached
      Boolean :detaching
      String :attached_status
      DateTime :attached_at
      Integer :size_gb
      String :availability_zone
    end
    
    DB.create_table :snapshots do
      primary_key :id
      DateTime :created_at
      DateTime :updated_at
      
      foreign_key :volume_id
      String :aws_id, :unique => true
      Boolean :deleted, :default => false
      Boolean :describing, :default => false
      Boolean :described, :default => false
      Boolean :available
      String :status
      DateTime :started_at
      Integer :volume_size
      String :progress
      String :description
    end
    
    DB.create_table :activity_logs do
      primary_key :id
      DateTime :created_at
      DateTime :updated_at
      File :content, :size => :tiny, :default => '', :null => false
    end
    
    DB.create_table :spot_request_logs do
      Integer :id
      foreign_key :spot_request_id
    end
    
    DB.create_table :instance_logs do
      Integer :id
      foreign_key :instance_id#, :null => false
    end
    
    DB.create_table :image_logs do
      Integer :id
      foreign_key :image_id#, :null => false
    end
    
    DB.create_table :volume_logs do
      Integer :id
      foreign_key :volume_id#, :null => false
    end
    
    DB.create_table :snapshot_logs do
      Integer :id
      foreign_key :snapshot_id#, :null => false
    end
    puts "DB created"
  end
  
  task :migrate do
    DB.alter_table :instances do
      add_column :old_volume_id, String
      add_column :old_snapshot_id, String
      add_column :old_image_id, String
    end
  end
  
  task :drop do
    DB.drop_table?(:spot_prices)
    DB.drop_table?(:spot_requests)
    DB.drop_table?(:instances)
    DB.drop_table?(:images)
    DB.drop_table?(:volumes)
    DB.drop_table?(:snapshots)
    DB.drop_table?(:activity_logs)
    DB.drop_table?(:spot_request_logs)
    DB.drop_table?(:instance_logs)
    DB.drop_table?(:image_logs)
    DB.drop_table?(:volume_logs)
    DB.drop_table?(:snapshot_logs)
    puts "DB dropped"
  end
  
  task :rebuild => [:drop, :create] do
    puts "DB rebuilt!"
  end
end
