module EC2Spotter
  module Cleaner
    
    class << self
      def run
        puts
        puts "---[ BEGINNING CLEANER RUN @ #{Time.now}]---"
        queue_jobs
=begin
        # queue any new instances to be "described" (get associated information)
        Database::Instance.select(:id, :instance_id).where(:described => false).each do |instance|
          Backburner.enqueue Job::DescribeInstance, instance.id
          puts "[DESCRIBE] Queued instance ##{instance.instance_id}..."
        end
        
        # queue any instances which we currently have marked as running
        # but are being reported as terminated by AWS
        running_instances = []
        response = Aws.ec2.describe_instance_status
        response.data.to_hash[:instance_statuses].each do |status|
          if status[:instance_state][:code] == 16
            running_instances.push(status[:instance_id])
          end
        end
        Database::Instance.select(:id, :instance_id).where(:described => true, :running => true).each do |instance|
          if !running_instances.include?(instance.instance_id)
            instance.update(:running => false, :relaunch_state => 0)
            Backburner.enqueue Job::CreateSnapshot, instance.id
            puts "[RELAUNCH] Queued instance ##{instance.instance_id}..."
          end
        end
=end
        puts "-" * 30
        puts
      end
      
    private
    
      def queue_jobs
        Volume.where(:deleted => false, :described => true, :instance => nil).each do |volume|
          puts "[DELETE] Volume ##{volume.id} (#{volume.aws_id})"
          Backburner.enqueue Job::DeleteVolume, volume.id
        end
        Snapshot.where(:deleted => false, :described => true, :volume => nil).each do |snapshot|
          puts "[DELETE] Snapshot ##{snapshot.id} (#{snapshot.aws_id})"
          Backburner.enqueue Job::DeleteSnapshot, snapshot.id
        end
        Image.where(:degregistered => false, :described => true, :instance => nil).each do |image|
          puts "[DELETE] Image ##{image.id} (#{image.aws_id})"
          Backburner.enqueue Job::DeregisterAMI, image.id
        end
      end
      
      def cleanup(instance_id)
        puts
        puts "---[CreateSnapshot(#{instance_id})]---"
        puts "Verifying the existence of instance '#{instance_id}'..."
        if instance = Database::Instance.first(:id => instance_id, :relaunch_state => 3)
          puts "Verifying the existence of spot request '#{instance.spot_request_id}'..."
          if spot_request = describe_spot_request(instance.spot_request_id)
            if spot_request[:instance_id] && spot_request[:status][:code] == 'fulfilled'
              if instance.old_image_id
                Aws.ec2.deregister_image(:image_id => instance.old_image_id)
                puts "Deregistered AMI: #{instance.old_image_id}"
              end
              sleep 10
              if instance.old_volume_id
                Aws.ec2.delete_volume(:volume_id => instance.old_volume_id)
                puts "Deleted volume: #{instance.old_volume_id}"
              end
              sleep 10
              if instance.old_snapshot_id
                Aws.ec2.delete_snapshot(:snapshot_id => instance.old_snapshot_id)
                puts "Deleted snapshot: #{instance.old_snapshot_id}"
              end
              sleep 10
              instance.update(:instance_id => spot_request[:instance_id], :described => false, 
                :running => nil, :relaunch_state => nil, :launch_time => nil, :public_dns => nil,
                :public_ip => nil, :instance_type => nil, :availability_zone => nil,
                :key_name => nil, :root_device => nil, :architecture => nil, :kernel_id => nil)
              puts "Relaunched instance: #{spot_request[:instance_id]}"
              Thread.new(Job::DescribeInstance, instance_id, 5) do |j,i,d|
                sleep d
                Backburner::Worker.enqueue(j, i)
              end.run
            else
              puts "Spot request ##{instance.spot_request_id} is not yet fulfilled! Retry in 30s..."
              Thread.new(Job::Cleanup, instance_id, 30) do |j,i,d|
                  sleep d
                  Backburner::Worker.enqueue(j, i)
                end.run
            end
          end
        else
          #self.bury
          raise StandardError, "Instance ##{instance_id} does not exist!"
        end
        puts "-" * 40
        puts
      end
      
    private
      def describe_spot_request(spot_request_id)
        response = Aws.ec2.describe_spot_instance_requests(:spot_instance_request_ids => [spot_request_id])
        response.data.to_hash[:spot_instance_requests][0]
      end
    end
    
  end
end