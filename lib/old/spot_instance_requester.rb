module EC2Spotter
  module SpotInstanceRequester
    
    class << self
      def request_spot_instance(instance_id)
        puts
        puts "---[RequestSpotInstance(#{instance_id})]---"
        puts "Verifying the existence of instance '#{instance_id}'..."
        if instance = Database::Instance.first(:id => instance_id, :relaunch_state => 2)
          puts "Verifying the existence of AMI '#{instance.image_id}'..."
          if image = describe_image(instance.image_id)
            if image[:state] == 'available'
              if spot_price = Database::SpotPrice.first(:availability_zone => instance.availability_zone,
                  :instance_type => instance.instance_type)
                response = Aws.ec2.request_spot_instances(
                  :spot_price => "#{spot_price[:price]+(spot_price[:price]*0.2)}", 
                  :launch_specification => {
                    :image_id => instance.image_id, 
                    :key_name => instance.key_name, 
                    :instance_type => instance.instance_type, 
                    :kernel_id => instance.kernel_id, 
                    :placement => {
                      :availability_zone => instance.availability_zone
                    }, 
                    :security_groups => ['base-sg']
                  })
                response = response.data.to_hash[:spot_instance_requests][0]
                if response[:spot_instance_request_id]
                  puts "Spot instance requested: #{response[:spot_instance_request_id]}"
                  instance.update(:spot_request_id => response[:spot_instance_request_id], :relaunch_state => 3)
                end
                Thread.new(Job::Cleanup, instance.id, 90) do |j,i,d|
                  sleep d
                  Backburner::Worker.enqueue(j, i)
                end.run
              else
                puts "Spot price for #{instance.availability_zone} #{instance.instance_type} is unavailable! Retry in 60s..."
                Thread.new(Job::RequestSpotInstance, instance_id, 300) do |j,i,d|
                  sleep d
                  Backburner::Worker.enqueue(j, i)
                end.run
              end
            else
              puts "Image ##{instance.image_id} is unavailable! Retry in 30s..."
              Thread.new(Job::RequestSpotInstance, instance_id, 30) do |j,i,d|
                  sleep d
                  Backburner::Worker.enqueue(j, i)
                end.run
            end
          end
        else
          #self.bury
          raise StandardError, "Instance ##{instance_id} does not exist!"
        end
        puts "-" * 40
        puts
      end
      
    private
      def describe_image(image_id)
        response = Aws.ec2.describe_images(:image_ids => [image_id])
        response.data.to_hash[:images][0]
      end
    end
    
  end
end