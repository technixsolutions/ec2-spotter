module EC2Spotter
  module AMIRegistration
    
    class << self
      def register_ami(instance_id)
        puts
        puts "---[RegisterAMI(#{instance_id})]---"
        puts "Verifying the existence of instance '#{instance_id}'..."
        if instance = Database::Instance.first(:id => instance_id, :relaunch_state => 1)
          puts "Verifying the existence of snapshot '#{instance.snapshot_id}'..."
          if snapshot = describe_snapshot(instance.snapshot_id)
            if snapshot[:state] == 'completed'
              response = Aws.ec2.register_image(
                :name => instance.instance_id, 
                :architecture => instance.architecture, 
                :root_device_name => instance.root_device, 
                :block_device_mappings => [{
                  :device_name => instance.root_device, 
                  :ebs => {
                    :snapshot_id => instance.snapshot_id, 
                    :volume_size => 8, 
                    :delete_on_termination => false
                  }
                }],
                :kernel_id => instance.kernel_id)
              response = response.data.to_hash
              if response[:image_id]
                puts "AMI registered: #{response[:image_id]}"
                instance.update(:image_id => response[:image_id], :relaunch_state => 2)
              end
              Thread.new(Job::RequestSpotInstance, instance.id, 10) do |j,i,d|
                  sleep d
                  Backburner::Worker.enqueue(j, i)
                end.run
            else
              puts "Snapshot ##{instance.snapshot_id} is not completed! Retry in 30s..."
              Thread.new(Job::RegisterAMI, instance_id, 30) do |j,i,d|
                  sleep d
                  Backburner::Worker.enqueue(j, i)
                end.run
            end
          end
        else
          #self.bury
          raise StandardError, "Instance ##{instance_id} does not exist!"
        end
        puts "-" * 40
        puts
      end
      
    private
      def describe_snapshot(snapshot_id)
        response = Aws.ec2.describe_snapshots(:snapshot_ids => [snapshot_id])
        response.data.to_hash[:snapshots][0]
      end
    end
    
  end
end