module EC2Spotter
  module SnapshotCreation
    
    class << self
      def create_snapshot_from_volume(instance_id)
        puts
        puts "---[CreateSnapshot(#{instance_id})]---"
        puts "Verifying the existence of instance '#{instance_id}'..."
        if instance = Database::Instance.first(:id => instance_id, :relaunch_state => 0)
          puts "Verifying the existence of volume '#{instance.volume_id}'..."
          if volume = describe_volume(instance.volume_id)
            if volume[:state] == 'available'
              puts "Volume is available! Attempting to create snapshot..."
              response = Aws.ec2.create_snapshot(:volume_id => instance.volume_id)
              response = response.data.to_hash
              if response[:snapshot_id] && response[:volume_id] == instance.volume_id
                puts "Snapshot created successfully: #{response[:snapshot_id]}"
                instance.update(:snapshot_id => response[:snapshot_id], :relaunch_state => 1)
              end
              Thread.new(Job::RegisterAMI, instance_id, 15) do |j,i,d|
                sleep d
                Backburner::Worker.enqueue(j, i)
              end.run
            else
              puts "Volume ##{instance.volume_id} is unavailable! Retry in 30s..."
              Thread.new(Job::CreateSnapshot, instance_id, 30) do |j,i,d|
                sleep d
                Backburner::Worker.enqueue(j, [i])
              end.run
            end
          end
        else
          #self.bury
          raise StandardError, "Instance ##{instance_id} does not exist!"
        end
        puts "-" * 40
        puts
      end
      
    private
      def describe_volume(volume_id)
        response = Aws.ec2.describe_volumes(:volume_ids => [volume_id])
        response.data.to_hash[:volumes][0]
      end
    end
    
  end
end