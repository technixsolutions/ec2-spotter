module EC2Spotter
  module InstanceDescription
    
    class << self
      def describe_instance(instance_id)
        puts
        puts "---[DescribeInstance(#{instance_id})]---"
        puts "Verifying the existence of instance '#{instance_id}'..."
        if instance = Database::Instance.first(:id => instance_id, :described => false)
          response = Aws.ec2.describe_instances(:instance_ids => [instance.instance_id])
          response = response.data.to_hash[:reservations][0][:instances][0]
          if response[:instance_id] == instance.instance_id
            if response[:state][:code] == 16
              if instance.launches > 0
                instance.update(:old_image_id => instance.image_id, :old_volume_id => instance.volume_id, :old_snapshot_id => instance.snapshot_id,
                  :spot_request_id => nil)
              end
              instance.update(:described => true, :launches => (instance.launches+1), :running => (response[:state][:code] == 16), :launch_time => response[:launch_time], 
                :public_dns => response[:public_dns_name], :public_ip => response[:public_ip_address], :instance_type => response[:instance_type], 
                :availability_zone => response[:placement][:availability_zone], :key_name => response[:key_name], 
                :root_device => response[:root_device_name], :architecture => response[:architecture], :kernel_id => response[:kernel_id], 
                :image_id => response[:image_id], :volume_id => response[:block_device_mappings][0][:ebs][:volume_id], :snapshot_id => nil)
              puts "Instance information recorded: #{response}"
            else
              puts "Should not describe an instance unless its status code is 16! Retry in 30s..."
              Thread.new(Job::DescribeInstance, instance_id, 30) do |j,i,d|
                  sleep d
                  Backburner::Worker.enqueue(j, i)
                end.run
            end
          else
            puts "Instance ##{instance.instance_id} is unavailable! Retry in 30s..."
            Thread.new(Job::DescribeInstance, instance_id, 30) do |j,i,d|
                  sleep d
                  Backburner::Worker.enqueue(j, i)
                end.run
          end
        else
          #self.bury
          raise StandardError, "Instance ##{instance_id} does not exist!"
        end
        puts "-" * 40
        puts
      end
    end
    
  end
end