module EC2Spotter
  class SpotPrice
    class SpotPriceNotFound < Exception; end
  end
  
  class Instance
    class InstanceNotFound < Exception; end
    class InstanceNonExistent < Exception; end
    class InstanceDescribed < Exception; end
    class InstanceUndescribed < Exception; end
    class InstanceUnavailable < Exception; end
  end
  
  class Image
    class ImageNotFound < Exception; end
    class ImageNonExistent < Exception; end
    class ImageDeregistered < Exception; end
    class ImageDescribed < Exception; end
    class ImageUndescribed < Exception; end
    class ImageUnavailable < Exception; end
  end
  
  class Volume
    class VolumeNotFound < Exception; end
    class VolumeNonExistent < Exception; end
    class VolumeDeleted < Exception; end
    class VolumeDescribed < Exception; end
    class VolumeUndescribed < Exception; end
    class VolumeUnavailable < Exception; end
  end
  
  class Snapshot
    class SnapshotNotFound < Exception; end
    class SnapshotNonExistent < Exception; end
    class SnapshotDeleted < Exception; end
    class SnapshotDescribed < Exception; end
    class SnapshotUndescribed < Exception; end
    class SnapshotUnavailable < Exception; end
  end
  
  class InvalidAWSResponseWarning < Exception; end
  class InvalidAWSResponseError < Exception; end
end