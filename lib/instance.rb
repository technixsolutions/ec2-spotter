module EC2Spotter
    
  class Instance < Database::Instance
    def describe(chain=true,queue_jobs=false)
      if chain
        Image[self.image.id].describe if self.image
        Volume[self.volume.id].describe if self.volume
      end
      
      raise InstanceNonExistent, "Instance ##{self.id} is not associated w/ an AWS ID!" if self.aws_id.nil?
      #raise InstanceDescribed, self.inspect if self.described
      
      puts "describing.."
      if response = Aws.ec2.describe_instances(:instance_ids => [self.aws_id]).data.to_hash
        response = response[:reservations][0][:instances][0]
        self.update(:described => false, :describing => true)
        
        puts "got response, updating instance..."
        self.aws_id = response[:instance_id]
        self.available = (response[:state][:code] == 16)
        self.terminated = (response[:state][:code] == 48)
        self.status = response[:state][:name]
        self.launch_time = response[:launch_time]
        self.instance_type = response[:instance_type]
        self.availability_zone = response[:placement][:availability_zone]
        self.key_name = response[:key_name]
        self.root_device = response[:root_device_name]
        self.architecture = response[:architecture]
        self.kernel_id = response[:kernel_id]
        if response[:state][:code] == 16
          #self.relaunches ||= 0
          #self.relaunches += 1
          #self.relaunch_state = nil
          self.public_ip = response[:public_ip_address]
          self.public_dns = response[:public_dns_name]
          #puts "checking image"
          #if self.image && response
          #  if self.image.aws_id != response[:image_id]
          #    Image[self.image.id].remove_instance
          #  end
          #end
          self.image = Image.find_or_create(:aws_id => response[:image_id])
          #puts "checking volume"
          #if self.volume
          #  if self.volume.aws_id != response[:block_device_mappings][0][:ebs][:volume_id]
          #    Volume[self.volume.id].remove_instance
          #  end
          #end
          self.volume = Volume.find_or_create(:aws_id => response[:block_device_mappings][0][:ebs][:volume_id])
        else # instance is terminated, stopped, stopping, or something else (not running)
          self.available = false
        end
        self.describing = false
        self.described = true
        self.save
        
        if queue_jobs
=begin
          if instance.image.described == false
            Backburner.enqueue Job::DescribeImage, instance.volume.id if queue_jobs
          end
          if instance.volume.described == false
            Backburner.enqueue Job::DescribeVolume, instance.volume.id if queue_jobs
          end
=end
        end
      
        self.log.record("INFO", "Successfully described Instance AMI ID ##{self.aws_id}...", inspect)
      else
        raise InvalidAWSResponseError, "Invalid response (nil) from Amazon for Instance AWS ID ##{self.aws_id}!"
      end
    rescue InstanceNonExistent => e
      self.log.record("WARNING", e.message, inspect)
    rescue InstanceDescribed => e
      self.log.record("ERROR", "Instance ##{id} is already set as DESCRIBED in the DB!", e.inspect)
    rescue InvalidAWSResponseWarning => e
      self.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      self.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidInstanceNotFound
      delete
      self.log.record("ERROR", "THIS INSTANCE IS NON-EXISTENT! Deleted from DB...")
    rescue => e
      puts e.backtrace
      raise "[ERROR] #{e.inspect}"
    end
    
    def relaunch
      describe
      raise Volume::VolumeNotFound, self.volume.inspect if !self.volume
      raise Volume::VolumeDeleted, self.volume.inspect if self.volume.deleted
      raise Volume::VolumeUndescribed, self.volume.inspect if self.volume.described == false
      raise Volume::VolumeUnavailable, self.volume.inspect if self.volume.available == false
      if self.response[:state][:code] == 48
        Backburner.enqueue Job::CreateSnapshot, self.volume.id
      end
    rescue Volume::VolumeNotFound => e
      self.log.record("ERROR", "Instance ##{id} has no associated Volume in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Volume::VolumeDeleted => e
      self.volume.log.record("ERROR", "Volume for Instance ##{id} is set as DELETED in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Volume::VolumeUndescribed => e
      self.volume.log.record("ERROR", "Volume for Instance ##{id} is set as UNDESCRIBED in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Volume::VolumeUnavailable => e
      self.volume.log.record("ERROR", "Volume for Instance ##{id} is set as UNAVAILABLE in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue => e
      self.log.record("ERROR", e.inspect, e.backtrace)
      raise "[ERROR] #{e.inspect}"
    end
    
    def register_image(queue_jobs=false)
      describe
      raise InstanceNonExistent, "Instance ##{id} is not associated w/ an AWS ID!" if aws_id.nil?
      raise InstanceUndescribed, self.inspect if self.described == false
      raise Volume::VolumeNotFound, self.volume.inspect if !self.volume
      raise Volume::VolumeDeleted, self.volume.inspect if self.volume.deleted
      raise Volume::VolumeUndescribed, self.volume.inspect if self.volume.described == false
      raise Volume::VolumeUnavailable, self.volume.inspect if self.volume.available == false
      snapshot = self.volume.snapshot
      raise Snapshot::SnapshotNotFound, snapshot.inspect if !snapshot
      raise Snapshot::SnapshotDeleted, snapshot.inspect if snapshot.deleted
      raise Snapshot::SnapshotUndescribed, snapshot.inspect if snapshot.described == false
      raise Snapshot::SnapshotUnavailable, snapshot.inspect if snapshot.available == false
      
      response = Aws.ec2.register_image(
        :name => "#{self.aws_id}_#{self.relaunches}_#{rand(1234567..45678900).to_s(36)}", 
        :architecture => self.architecture, 
        :root_device_name => self.root_device, 
        :block_device_mappings => [{
          :device_name => self.root_device, 
          :ebs => {
            :snapshot_id => snapshot.aws_id, 
            :volume_size => 8, 
            :delete_on_termination => false
          }
        }],
        :kernel_id => self.kernel_id).data.to_hash
      if !response[:image_id].nil?
        if image = Image.create(:aws_id => response[:image_id], :snapshot => snapshot)
        #if image = Image.find_or_create(:aws_id => response[:image_id])
          #image.snapshot = snapshot
          #image.save
          self.image = image
          self.save
          
          Image[self.image.id].describe if self.image
          
          if queue_jobs
=begin
            if image.described == false
              Backburner.enqueue Job::DescribeImage, image.id
            end
=end
            Backburner.enqueue Job::RequestSpotInstance, self.id
          end
          
          #puts "AMI registered: #{response[:image_id]}"
          snapshot.log.record("INFO", "Successfully registered AMI AWS ID ##{response[:image_id]}...", self.image.inspect)
          self.log.record("INFO", "Successfully registered Image AWS ID ##{response[:image_id]}...", self.image.inspect)
        else
          raise "Could not create Image!"
        end
      else
        raise InvalidAWSResponseWarning, "A valid AWS ID was not returned when trying to create AMI from Instance AWS ID ##{self.aws_id}"
      end
    rescue InstanceNotFound, InstanceNonExistent => e
        puts "[ERROR] #{e.inspect}"
    rescue InstanceUndescribed => e
      self.log.record("ERROR", "Instance ##{id} is set as UNDESCRIBED in the DB!", e.message)
      Backburner.enqueue Job::DescribeInstance, self.id if queue_jobs
    rescue InstanceUnavailable => e
      self.log.record("ERROR", "Instance ##{id} is set as UNAVAILABLE in the DB!", e.message)
    rescue Volume::VolumeNotFound => e
      self.log.record("ERROR", "Instance ##{id} has no associated Volume in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Volume::VolumeDeleted => e
      self.volume.log.record("ERROR", "Volume for Instance ##{id} is set as DELETED in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Volume::VolumeUndescribed => e
      self.volume.log.record("ERROR", "Volume for Instance ##{id} is set as UNDESCRIBED in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
      Backburner.enqueue Job::DescribeVolume, self.volume.id if queue_jobs
    rescue Volume::VolumeUnavailable => e
      self.volume.log.record("ERROR", "Volume for Instance ##{id} is set as UNAVAILABLE in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Snapshot::SnapshotNotFound => e
      self.log.record("ERROR", "Instance ##{id} has no associated Snapshot in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Snapshot::SnapshotDeleted => e
      snapshot.log.record("ERROR", "Snapshot for Instance ##{id} is set as DELETED in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Snapshot::SnapshotUndescribed => e
      snapshot.log.record("ERROR", "Snapshot for Instance ##{id} is set as UNDESCRIBED in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
      Backburner.enqueue Job::DescribeSnapshot, self.volume.id if queue_jobs
    rescue Snapshot::SnapshotUnavailable => e
      snapshot.log.record("ERROR", "Snapshot for Instance ##{id} is set as UNAVAILABLE in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidSnapshotNotFound => e
      #snapshot.deleted = true
      #snapshot.available = false
      #snapshot.status = 'deleted'
      #snapshot.save
      #snapshot.log.record("WARNING", "THIS SNAPSHOT IS NON-EXISTENT ON AWS! Set as deregistered...")
      snapshot.delete
      self.image.log.record("ERROR", "AMI AWS ID ##{self.image.aws_id} is non-existent on Amazon!")
      self.log.record("ERROR", "AMI AWS ID ##{self.image.aws_id} is non-existent on Amazon!")
    rescue InvalidAWSResponseWarning => e
      self.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      self.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue => e
      self.log.record("ERROR", e.inspect, e.backtrace)
      raise "[ERROR] #{e.inspect}"
    end
    
    def request_spot_instance(queue_jobs=false)
      describe
      raise InstanceNonExistent, "Instance ##{id} is not associated w/ an AWS ID!" if self.aws_id.nil?
      raise InstanceUndescribed, self.inspect if self.described == false
      raise Image::ImageNotFound, self.image.inspect if !self.image
      raise Image::ImageNonExistent, self.image.inspect if self.image.aws_id.nil?
      spot_price = Database::SpotPrice.first(:availability_zone => self.availability_zone,
                  :instance_type => self.instance_type)
      raise SpotPrice::SpotPriceNotFound, self.inspect if !spot_price 
      
      if self.image && self.volume && self.terminated
        response = Aws.ec2.request_spot_instances(
          :spot_price => "#{spot_price[:price]+(spot_price[:price]*0.2)}",
          :launch_specification => {
            :image_id => self.image.aws_id, 
            :key_name => self.key_name, 
            :instance_type => self.instance_type, 
            :kernel_id => self.kernel_id, 
            :placement => {
              :availability_zone => self.availability_zone
            }, 
            :security_groups => ['base-sg']
          }).data.to_hash[:spot_instance_requests][0]
        if !response[:spot_instance_request_id].nil?
          if spot_request = SpotRequest.create(:aws_id => response[:spot_instance_request_id])
            #self.aws_id = nil
            #self.describing = false
            #self.described = false
            #self.available = nil
            #self.status = nil
            #self.relaunches = 0
            #self.relaunch_state = nil
            #self.spot_request_id = response[:spot_instance_request_id]
            #self.launch_time = nil
            #self.instance_type = nil
            #self.availability_zone = nil
            #self.key_name = nil
            #self.root_device = nil
            #self.architecture = nil
            #self.kernel_id = nil
            #self.public_ip = nil
            #self.public_dns = nil
            #self.image = nil
            #self.volume = nil
            self.relaunched = true
            self.save
            
            SpotRequest[spot_request.id].describe
            
            if queue_jobs
              #Backburner.enqueue Job::DescribeInstance, self.id
            end
            
            self.new_log.record("INFO", "Successfully requested spot instance: #{response[:spot_instance_request_id]}")
          else
            raise "Failed to create SpotRequest!"
          end
        else
          raise InvalidAWSResponseWarning, "An invalid response was received when trying to make Spot Request for Instance AWS ID ##{self.aws_id}"
        end
      else
        raise "Cannot request spot instance (instance is missing image, volume, or is set as AVAILABLE in the DB)"
      end
    rescue InstanceNotFound, InstanceNonExistent => e
        puts "[ERROR] #{e.inspect}"
    rescue InstanceUndescribed => e
      self.image.log.record("ERROR", "Instance ##{id} is set as UNDESCRIBED in the DB!", e.message)
      Backburner.enqueue Job::DescribeInstance, self.id if queue_jobs
    rescue InstanceUnavailable => e
      self.image.log.record("ERROR", "Instance ##{id} is set as UNAVAILABLE in the DB!", e.message)
    rescue Volume::VolumeNotFound => e
      self.image.log.record("ERROR", "Instance ##{id} has no associated Volume in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Volume::VolumeDeleted
      self.image.log.record("ERROR", "Volume for Instance ##{id} is set as DELETED in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
    rescue Volume::VolumeUndescribed
      self.image.log.record("ERROR", "Volume for Instance ##{id} is set as UNDESCRIBED in the DB!", e.message)
      raise "[ERROR] #{e.inspect}"
      Backburner.enqueue Job::DescribeVolume, self.volume.id if queue_jobs
    rescue InvalidAWSResponseWarning => e
      self.image.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      self.image.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidImageNotFound
      self.image.deregistered = true
      self.image.available = false
      self.image.status = 'deregistered'
      self.image.attaching = false
      self.image.attached = false
      self.image.detaching = false
      self.image.attached_status = 'deregistered'
      self.image.save
      self.log.record("ERROR", "AMI AWS ID ##{self.image.aws_id} is non-existent on Amazon!")
      self.image.log.record("WARNING", "AMI AWS ID ##{self.image.aws_id} is non-existent on Amazon! Set as deregistered...")
    rescue => e
      self.image.log.record("ERROR", e.inspect, e.backtrace)
      raise "[ERROR] #{e.inspect}"
    end
  end
  
end