module EC2Spotter
    
  class Snapshot < Database::Snapshot
    def describe(chain=true,queue_jobs=false)
      raise SnapshotNonExistent, "Snapshot ##{self.id} is not associated w/ an AWS ID!" if self.aws_id.nil?
      #raise SnapshotDescribed, self.inspect if self.described
      
      self.update(:describing => true)
      if response = Aws.ec2.describe_snapshots(:snapshot_ids => [self.aws_id]).data.to_hash[:snapshots][0]
        self.aws_id = response[:snapshot_id]
        self.available = (response[:state] == 'completed')
        self.status = response[:state]
        self.started_at = response[:start_time]
        self.progress = response[:progress]
        self.volume_size = response[:volume_size]
        self.description = response[:description]
        self.volume = Volume.find_or_create(:aws_id => response[:volume_id]) if !volume
        self.describing = false
        self.described = true
        self.save
        
        if chain
          Volume[self.volume.id].describe if self.volume
        end
        
        if queue_jobs
=begin
          if snapshot.volume.described == false
            Backburner.enqueue Job::DescribeVolume, snapshot.volume.id
          end
=end
        end
        
        self.log.record("INFO", "Successfully described Snapshot AWS ID ##{aws_id}", self.inspect)
      else
        raise InvalidAWSResponseError, "Invalid response (nil) from Amazon for Snapshot AWS ID ##{self.aws_id}!"
      end
    rescue SnapshotNotFound, SnapshotNonExistent => e
      puts "[ERROR] #{e.inspect}"
    rescue SnapshotDescribed => e
      self.log.record("ERROR", "Snapshot ##{id} is already set as DESCRIBED in the DB!", e.inspect)
    rescue InvalidAWSResponseWarning => e
      self.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      self.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidSnapshotNotFound
      #self.log.record("ERROR", "THIS SNAPSHOT IS NON-EXISTENT ON AWS! Set to deleted...", inspect)
      #self.deleted = true
      #self.available = false
      #self.status = 'deleted'
      #self.attaching = false
      #self.attached = false
      #self.detaching = false
      #self.attached_status = 'deleted'
      #self.save
      self.log.record("ERROR", "Snapshot not found!")
      self.remove_volume
      self.delete
    rescue => e
      raise "Uncaught exception: #{e.inspect}"
    end
    
    def delete_snapshot(queue_jobs=false)
      self.describe(id)
      raise SnapshotUndescribed, snapshot.inspect if snapshot.described == false
      raise SnapshotDeleted, snapshot.inspect if snapshot.deleted
      raise SnapshotUnavailable, snapshot.inspect if snapshot.available == false
      Aws.ec2.delete_snapshot(:snapshot_id => snapshot.aws_id)
    rescue SnapshotUndescribed => e
      snapshot.log.record("ERROR", "Snapshot ##{id} is already set as DESCRIBED in the DB!", e.inspect)
    rescue SnapshotDeleted => e
      snapshot.log.record("ERROR", "Snapshot ##{id} is already set as DELETED in the DB!", e.inspect)
    rescue SnapshotUnavailable => e
      snapshot.log.record("ERROR", "Snapshot ##{id} is already set as UNAVAILABLE in the DB!", e.inspect)
    rescue InvalidAWSResponseWarning => e
      snapshot.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      snapshot.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidSnapshotNotFound
      #snapshot.log.record("ERROR", "THIS SNAPSHOT IS NON-EXISTENT ON AWS! Set to deleted...", snapshot)
      #snapshot.deleted = true
      #snapshot.available = false
      #snapshot.status = 'deleted'
      #snapshot.attaching = false
      #snapshot.attached = false
      #snapshot.detaching = false
      #snapshot.attached_status = 'deleted'
      #snapshot.save
      self.log.record("ERROR", "Snapshot not found!")
      self.remove_volume
      self.delete
    rescue => e
      raise "Uncaught exception: #{e.inspect}"
    end
    
  end
end