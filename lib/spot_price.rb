module EC2Spotter
    
  class SpotPrice < Database::SpotPrice
    class << self
      def describe(availability_zones, instance_types)
        availability_zones.each do |availability_zone|
          response = Aws.ec2.describe_spot_price_history(:instance_types => instance_types, :availability_zone => availability_zone, 
            :product_descriptions => ['Linux/UNIX'])
          prices = response.data.to_hash[:spot_price_history]
          
          encountered_types = []
          prices.each do |price|
            if !encountered_types.include?(price[:instance_type])
              encountered_types.push(price[:instance_type])
              spot_price = Database::SpotPrice.first(:availability_zone => price[:availability_zone], :instance_type => price[:instance_type])
              spot_price ||= Database::SpotPrice.create(:availability_zone => price[:availability_zone], :instance_type => price[:instance_type])
              spot_price.update(:aws_timestamp => price[:timestamp], :price => price[:spot_price])
              puts "[SPOT PRICE] Recorded most recent spot price (#{price[:spot_price]}) for #{availability_zone} #{price[:instance_type]}..."
            end
          end
        end
      rescue => e
        raise "Uncaught exception: #{e}"
      end
    end
  end
  
end