module EC2Spotter
    
  class SpotRequest < Database::SpotRequest
      def describe(queue_jobs=false)
        self.update(:describing => true)
        if response = Aws.ec2.describe_spot_instance_requests(:spot_instance_request_ids => [self.aws_id]).data.to_hash[:spot_instance_requests][0]
          self.available = (response[:state] == 'active')
          self.cancelled = (response[:state] == 'cancelled' || response[:state] == 'closed')
          self.status = response[:state]
          self.price = response[:spot_price]
          if response[:state] == 'active'
            self.instance = Instance.find_or_create(:aws_id => response[:instance_id])
          end
          self.describing = false
          self.described = true
          self.save
          
          #if self.available
            Instance[self.instance.id].describe if self.instance
            
            if queue_jobs
=begin
              if snapshot.volume.described == false
                Backburner.enqueue Job::DescribeVolume, snapshot.volume.id
              end
=end
            end
          
            self.log.record("INFO", "Successfully described Snapshot AWS ID ##{aws_id}", self.inspect)
          #else
          #  puts "Spot request has not yet been fulfilled!"
          #end
        else
          raise InvalidAWSResponseError, "Invalid response (nil) from Amazon for Snapshot AWS ID ##{self.aws_id}!"
        end
      rescue InvalidAWSResponseWarning => e
        self.log.record("WARNING", e.inspect, response)
      rescue InvalidAWSResponseError => e
        self.log.record("ERROR", e.inspect)
        raise "[ERROR] #{e.inspect}"
      rescue Aws::EC2::Errors::InvalidSpotRequestNotFound
        self.log.record("ERROR", "THIS SPOT REQUEST IS NON-EXISTENT ON AWS! Set to deleted...", inspect)
        self.deleted = true
        self.available = false
        self.status = 'deleted'
        self.attaching = false
        self.attached = false
        self.detaching = false
        self.attached_status = 'deleted'
        self.save
      rescue => e
        puts e.backtrace
        raise "Uncaught exception: #{e.inspect}"
      end
      
      def cleanup
        if self.instance.volume
          puts "Delete volume..."
          begin
          AWS.ec2.delete_volume(:olume_id => self.instance.volume.id) rescue nil
          rescue
            puts "Cannot delete volume!"
          end
          puts "Delete snapshot..."
          begin
            AWS.ec2.delete_snapshot(:snapshot_id => self.instance.volume.snapshot.id) 
          rescue
            puts "Cannot delete snapshot!"
          end
        else
          self.delete
        end
        #if self.instance.image
        #  AWS.ec2.deregister_image(:image_id => self.instance.image.id) if self.instance.image.
        #end
      end
    end
  
end