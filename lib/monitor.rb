module EC2Spotter
  module Monitor
    
    class << self
      def run
        puts
        puts "---[ BEGINNING MONITOR RUN @ #{Time.now}]---"
        queue_jobs
=begin
        # queue any new instances to be "described" (get associated information)
        Database::Instance.select(:id, :instance_id).where(:described => false).each do |instance|
          Backburner.enqueue Job::DescribeInstance, instance.id
          puts "[DESCRIBE] Queued instance ##{instance.instance_id}..."
        end
        
        # queue any instances which we currently have marked as running
        # but are being reported as terminated by AWS
        running_instances = []
        response = Aws.ec2.describe_instance_status
        response.data.to_hash[:instance_statuses].each do |status|
          if status[:instance_state][:code] == 16
            running_instances.push(status[:instance_id])
          end
        end
        Database::Instance.select(:id, :instance_id).where(:described => true, :running => true).each do |instance|
          if !running_instances.include?(instance.instance_id)
            instance.update(:running => false, :relaunch_state => 0)
            Backburner.enqueue Job::CreateSnapshot, instance.id
            puts "[RELAUNCH] Queued instance ##{instance.instance_id}..."
          end
        end
=end
        puts "-" * 30
        puts
      end
      
    private
    
      def queue_jobs
        SpotRequest.where(:cancelled => false).each do |spot_request|
          puts "[SPOT REQUEST] Queueing ##{spot_request.id} for description..."
          Backburner.enqueue Job::DescribeSpotRequest, spot_request.id
        end
        SpotRequest.where(:available => true).where('instance_id IS NOT NULL').each do |spot_request|
            if !spot_request.instance.relaunched && spot_request.instance.terminated
              puts "[INSTANCE] Queueing ##{spot_request.id} for relaunch..."
              Backburner.enqueue Job::RelaunchSpotRequest, spot_request.id
            end
        end
        SpotRequest.where(:cancelled => true).each do |spot_request|
          #puts "[SPOT REQUEST] Queueing ##{spot_request.id} for cleanup..."
          #Backburner.enqueue Job::Cleanup, spot_request.id
        end
        #Instance.each do |instance|
        #  Backburner.enqueue Job::DescribeInstance, instance.id
        #end
        #Snapshot.where(:available => true).each do |snapshot|
        #  Backburner.enqueue Job::DeleteSnapshot, snapshot.id if !snapshot.volume# && snapshot.available
        #end
        #Image.where(:available => true).each do |image|
        #  Backburner.enqueue Job::DeregisterAMI, image.id if !image.instance# && image.available
        #end
        #Volume.where(:available => true).each do |volume|
        #  Backburner.enqueue Job::DeleteVolume, volume.id if !volume.instance# && volume.available
        #end
        #Instance.where(:available => false).each do |instance|
        #  #instance.relaunch
        #  puts "[INSTANCE] Queueing ##{instance.id} for relaunch..."
        #  Backburner.enqueue Job::RelaunchSpotRequest, instance.spot_request.id if instance.spot_request# && instance.volume.available
        #end
      end
=begin
        Instance.where(:described => false, :describing => false).each do |instance|
          puts "[DESCRIBE] Instance ##{instance.id} (#{instance.aws_id})"
          Backburner.enqueue Job::DescribeInstance, instance.id
        end
        Image.where(:described => false, :describing => false).each do |image|
          puts "[DESCRIBE] Image ##{image.id} (#{image.aws_id})"
          Backburner.enqueue Job::DescribeImage, image.id
        end
        Volume.where(:described => false, :describing => false).each do |volume|
          puts "[DESCRIBE] Volume ##{volume.id} (#{volume.aws_id})"
          Backburner.enqueue Job::DescribeVolume, volume.id
        end
        Snapshot.where(:described => false, :describing => false).each do |snapshot|
          puts "[DESCRIBE] Snapshot ##{snapshot.id} (#{snapshot.aws_id})"
          Backburner.enqueue Job::DescribeSnapshot, snapshot.id
        end
      end
=end
    end
    
  end
end