module EC2Spotter
    
  class Volume < Database::Volume    
    def describe(chain=true,queue_jobs=false)
      raise VolumeNonExistent, "Volume ##{id} is not associated w/ an AWS ID!" if self.aws_id.nil?
      
      if response = Aws.ec2.describe_volumes(:volume_ids => [self.aws_id]).data
        response = response.to_hash[:volumes][0]
        self.update(:described => false, :describing => true)
        
        self.aws_id = response[:volume_id]
        self.available = (response[:state] == 'available')
        self.status = response[:state]
        self.size_gb = response[:size]
        self.availability_zone = response[:availability_zone]
        if response[:attachments].count > 0
          self.attaching = (response[:attachments][0][:state] == 'attaching')
          self.detaching = (response[:attachments][0][:state] == 'detaching')
          self.attached = (response[:attachments][0][:state] == 'attached')
          self.attached_status = response[:attachments][0][:state]
          self.attached_at = response[:attachments][0][:attach_time]
        else
          self.attaching = nil
          self.detaching = nil
          self.attached = nil
          self.attached_status = nil
          self.attached_at = nil
        end
        self.describing = false
        self.described = true
        self.save
        
        if chain
          Snapshot[self.snapshot.id].describe(false) if self.snapshot
        end
                
        if queue_jobs
          Backburner.enqueue Job::RegisterAMI, self.instance.id
        end
        
        self.log.record("INFO", "Successfully described Volume AWS ID ##{self.aws_id}...")
      else
        raise InvalidAWSResponseError, "Invalid response (nil) from Amazon for Volume AWS ID ##{self.aws_id}!"
      end
    rescue VolumeNotFound, VolumeNonExistent => e
      puts "[ERROR] #{e.inspect}"
    rescue VolumeDescribed => e
      self.log.record("ERROR", "Volume ##{id} is already set as DESCRIBED in the DB!", e.inspect)
    rescue InvalidAWSResponseWarning => e
      self.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      self.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidVolumeNotFound
      #self.log.record("ERROR", "THIS VOLUME IS NON-EXISTENT ON AWS! Set to deleted...", self.inspect)
      #self.deleted = true
      #self.available = false
      #self.status = 'deleted'
      #self.attaching = false
      #self.attached = false
      #self.detaching = false
      #self.attached_status = 'deleted'
      #self.save
      self.log.record("ERROR", "Volume not found!")
      self.instance.remove_volume
      self.delete
    rescue => e
      raise "Uncaught exception: #{e.inspect}"
    end
    
    def create_snapshot(queue_jobs=false)
      describe
      raise VolumeNonExistent, "Volume ##{id} is not associated w/ an AWS ID!" if aws_id.nil?
      raise VolumeUndescribed, inspect if described == false
      raise VolumeDeleted, inspect if deleted
      raise VolumeUnavailable, inspect if available == false
      
      response = Aws.ec2.create_snapshot(:volume_id => aws_id).data.to_hash
      if response[:volume_id] == aws_id && !response[:snapshot_id].nil?
        if snapshot = Snapshot.create(:aws_id => response[:snapshot_id], :volume => self)
          snapshot.describe
          
          if queue_jobs
=begin
            if snapshot.described == false
              Backburner.enqueue Job::DescribeSnapshot, snapshot.id
            end
=end
            Backburner.enqueue Job::RegisterAMI, instance.id
          end
          
          snapshot.log.record("INFO", "Successfully created this Snapshot from Volume AWS ID ##{aws_id}...")
          log.record("INFO", "Successfully created Snapshot ##{response[:snapshot_id]} from this Volume...")
        else
          log.record("ERROR", "Failed to find or create Snapshot AWS ID ##{response[:snapshot_id]}!")
        end
      end
    rescue VolumeNotFound, VolumeNonExistent => e
      puts "[ERROR] #{e.inspect}"
    rescue VolumeUndescribed => e
      log.record("ERROR", "Volume ##{id} is set as UNDESCRIBED in the DB!", e.message)
      #Backburner.enqueue Job::DescribeVolume, id if queue_jobs
    rescue VolumeDeleted => e
      log.record("ERROR", "Volume ##{id} is set as DELETED in the DB!", e.message)
      #Backburner.enqueue Job::DescribeVolume, id if queue_jobs
    rescue VolumeUnavailable => e
      log.record("ERROR", "Volume ##{id} is set as UNAVAILABLE in the DB!", e.message)
      #Backburner.enqueue Job::DescribeVolume, id if queue_jobs
    rescue InvalidAWSResponseWarning => e
      log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidVolumeNotFound
      #log.record("ERROR", "THIS VOLUME IS NON-EXISTENT ON AWS! Set to deleted...", inspect)
      #deleted = true
      #available = false
      #status = 'deleted'
      #attaching = false
      #attached = false
      #detaching = false
      #attached_status = 'deleted'
      #save
      self.log.record("ERROR", "Volume not found!")
      self.instance.remove_volume
      self.delete
    rescue => e
      raise "Uncaught exception: #{e.inspect}"
    end
    
    def delete_volume(queue_jobs=false)
      describe
      raise VolumeUndescribed, self.inspect if self.described == false
      raise VolumeDeleted, self.inspect if self.deleted
      raise VolumeUnavailable, self.inspect if self.available == false
      Aws.ec2.delete_volume(:volume_id => self.aws_id)
    rescue VolumeUndescribed => e
      self.log.record("ERROR", "Volume ##{id} is set as UNDESCRIBED in the DB!", e.message)
      Backburner.enqueue Job::DescribeVolume, self.id if queue_jobs
    rescue VolumeDeleted => e
      self.log.record("ERROR", "Volume ##{id} is set as DELETED in the DB!", e.message)
      Backburner.enqueue Job::DescribeVolume, self.id if queue_jobs
    rescue VolumeUnavailable => e
      self.log.record("ERROR", "Volume ##{id} is set as UNAVAILABLE in the DB!", e.message)
      Backburner.enqueue Job::DescribeVolume, self.id if queue_jobs
    rescue InvalidAWSResponseWarning => e
      self.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      self.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidVolumeNotFound
      #self.log.record("ERROR", "THIS VOLUME IS NON-EXISTENT ON AWS! Set to deleted...", volume)
      #self.deleted = true
      #self.available = false
      #self.status = 'deleted'
      #self.attaching = false
      #self.attached = false
      #self.detaching = false
      #self.attached_status = 'deleted'
      #self.save
      self.log.record("ERROR", "Volume not found!")
      self.instance.remove_volume
      self.delete
    rescue => e
      raise "Uncaught exception: #{e.inspect}"
    end
  end
  
end