module EC2Spotter
    
  class Image < Database::Image
    def describe(chain=true,queue_jobs=false)
      raise ImageNonExistent, "Image ##{self.id} is not associated w/ an AWS ID!" if self.aws_id.nil?
      #raise ImageDescribed, self.inspect if self.described
      
      self.update(:describing => true)
      if response = Aws.ec2.describe_images(:image_ids => [self.aws_id]).data.to_hash[:images][0]
        self.aws_id = response[:image_id]
        self.available = (response[:state] == 'available')
        self.status = response[:state]
        self.root_device = response[:block_device_mappings][0][:device_name]
        self.volume_size = response[:block_device_mappings][0][:ebs][:volume_size]
        self.architecture = response[:architecture]
        self.kernel_id = response[:kernel_id]
        self.description = response[:description]            
        self.snapshot = Snapshot.find_or_create(:aws_id => response[:block_device_mappings][0][:ebs][:snapshot_id]) if !self.snapshot
        self.describing = false
        self.described = true
        self.save
        
        if chain
          Snapshot[self.snapshot.id].describe(false) if self.snapshot
        end
       
        if queue_jobs
=begin
          if snapshot.described == false
            Backburner.enqueue Job::DescribeSnapshot, snapshot.id
          end
=end
        end
        
        self.log.record("INFO", "Successfully described Image AWS ID ##{aws_id}...", self.inspect)
      else
        raise InvalidAWSResponseError, "Invalid response (nil) from Amazon for Image AWS ID ##{aws_id}!"
      end
    rescue ImageNotFound, ImageNonExistent => e
      puts "[ERROR] #{e.inspect}"
    rescue ImageDescribed => e
      self.log.record("ERROR", "Image ##{id} is already set as DESCRIBED in the DB!", e.inspect)
    rescue InvalidAWSResponseWarning => e
      self.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      self.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidImageNotFound
      self.deregistered = true
      self.available = false
      self.status = 'deregistered'
      self.attaching = false
      self.attached = false
      self.detaching = false
      self.attached_status = 'deregistered'
      self.save
      self.log.record("WARNING", "AMI AWS ID ##{image.aws_id} is non-existent on Amazon! Set as deregistered...")
    rescue => e
      self.log.record("ERROR", e.inspect, e.backtrace)
      raise "[ERROR] #{e.inspect}"
    end
    
    def deregister(queue_jobs=false)
      raise ImageDeregistered, self.inspect if self.deregistered
      raise ImageUndescribed, self.inspect if self.described == false
      raise ImageUnavailable, self.inspect if self.available == false
      if response = Aws.ec2.deregister_image(:image_id => self.aws_id)
        self.log.record("INFO", "Successfully deregistered Image AWS ID ##{self.aws_id}...", response)
      else
        raise InvalidAWSResponseError, "Invalid response (nil) from Amazon for Image AWS ID ##{self.aws_id}!"
      end
    rescue ImageNotFound, ImageNonExistent => e
      puts "[ERROR] #{e.inspect}"
    rescue ImageDeregistered => e
      self.log.record("ERROR", "Image ##{id} is set as DEREGISTERED in the DB!", self.inspect)
    rescue ImageUndescribed => e
      self.log.record("ERROR", "Image ##{id} is set as UNDESCRIBED in the DB!", self.inspect)
      Backburner.enqueue Job::DescribeImage, self.id if queue_jobs
    rescue ImageUnavailable => e
      self.log.record("ERROR", "Image ##{id} is set as UNAVAILABLE in the DB!", self.inspect)
    rescue InvalidAWSResponseWarning => e
      self.log.record("WARNING", e.inspect, response)
    rescue InvalidAWSResponseError => e
      self.log.record("ERROR", e.inspect)
      raise "[ERROR] #{e.inspect}"
    rescue Aws::EC2::Errors::InvalidImageNotFound
      self.deregistered = true
      self.available = false
      self.status = 'deleted'
      self.attaching = false
      self.attached = false
      self.detaching = false
      self.attached_status = 'deleted'
      self.save
      self.log.record("WARNING", "THIS IMAGE IS NON-EXISTENT ON AWS! Set as deregistered...")
      instance.log.record("ERROR", "Image AWS ID ##{self.aws_id} is non-existent on AWS!")
    rescue => e
      self.log.record("ERROR", e.inspect, e.backtrace)
      raise "[ERROR] #{e.inspect}"
    end
  end
  
end