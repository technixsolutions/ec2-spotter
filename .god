God.watch do |w|
	w.name = "heartbeat-worker"
	w.group = "heartbeat"
   
	w.dir = "/root/ec2-spotter"
	#w.log = "/tmp/heartbeat-#{Time.now.strftime("started_%m-%d-%Y_%H-%M-%S")}.log"
	w.start = "bundle exec rake worker:run"
	
	w.keepalive(:memory_max => 400.megabytes, :cpu_max => 70.percent)
	w.lifecycle do |on|
		on.condition(:flapping) do |c|
			c.from_state = :start
			c.times = 2
			c.within = 5.minute
			c.transition = :unmonitored
			c.retry_times = 0
		end
	end
end