module EC2Spotter
  module Database
    
    class SnapshotLog < ActivityLog
      #plugin :class_table_inheritance
      
      many_to_one :snapshot
    end
    
  end
end