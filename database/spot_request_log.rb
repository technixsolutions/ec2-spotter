module EC2Spotter
  module Database
    
    class SpotRequestLog < ActivityLog
      #plugin :class_table_inheritance
      
      many_to_one :spot_request
    end
    
  end
end