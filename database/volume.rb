module EC2Spotter
  module Database
    
    class Volume < Sequel::Model
      include Loggable
      
      plugin :timestamps, :create => :created_at, :update => :updated_at
      
      one_to_many :logs, :class => :"EC2Spotter::Database::VolumeLog", :key => :volume_id
      one_to_one :instance
      one_to_one :snapshot
      
      def after_create
        new_log
      end
      
      def after_save
        reload
        true
      end
      
      def before_delete
        instance.remove_volume if instance
        snapshot.remove_volume if snapshot
        logs.each { |l| l.delete }
      end
    end
    
  end
end