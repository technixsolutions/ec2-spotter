module EC2Spotter
  module Database
    
    class ActivityLog < Sequel::Model
      include Logger
      
      plugin :timestamps, :create => :created_at, :update => :updated_at
      plugin :class_table_inheritance
    end
    
  end
end