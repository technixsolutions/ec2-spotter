module EC2Spotter
  module Database
    
    class VolumeLog < ActivityLog
      #plugin :class_table_inheritance
      
      many_to_one :volume
    end
    
  end
end