module EC2Spotter
  module Database
    
    class ImageLog < ActivityLog
      many_to_one :image
    end
    
  end
end