module EC2Spotter
  module Database
    
    class SpotRequest < Sequel::Model
      include Loggable
      
      plugin :timestamps, :create => :created_at, :update => :updated_at
      
      one_to_many :logs, :class => :"EC2Spotter::Database::SpotRequestLog", :key => :spot_request_id
      many_to_one :instance
      
      def after_create
        new_log
      end
      
      def after_save
        reload
        true
      end
    end
    
  end
end