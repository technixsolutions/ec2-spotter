module EC2Spotter
  module Database
    
    class InstanceLog < ActivityLog
      many_to_one :instance
    end
    
  end
end