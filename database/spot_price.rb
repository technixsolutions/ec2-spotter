module EC2Spotter
  module Database
    
    class SpotPrice < Sequel::Model
      plugin :timestamps, :create => :created_at, :update => :updated_at
      
      def after_save
        reload
        true
      end
    end
    
  end
end