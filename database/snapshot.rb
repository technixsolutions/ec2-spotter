module EC2Spotter
  module Database
    
    class Snapshot < Sequel::Model
      include Loggable
      
      plugin :timestamps, :create => :created_at, :update => :updated_at
      
      one_to_many :logs, :class => :"EC2Spotter::Database::SnapshotLog", :key => :snapshot_id
      one_to_one :image
      many_to_one :volume#, :before_set => proc { |o, t| false if o.aws_id.nil? }
      
      def after_create
        new_log
      end
      
      def after_save
        reload
        true
      end
      
      def before_delete
        remove_image if image
        remove_volume if volume
        logs.each { |l| l.delete }
      end
    end
    
  end
end