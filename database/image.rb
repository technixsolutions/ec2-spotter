module EC2Spotter
  module Database
    
    class Image < Sequel::Model
      include Loggable
      
      plugin :timestamps, :create => :created_at, :update => :updated_at
      
      one_to_many :logs, :class => :"EC2Spotter::Database::ImageLog", :key => :image_id
      one_to_one :instance
      many_to_one :snapshot
      
      def after_create
        new_log
      end
      
      def before_delete
        instance.remove_image if instance
        remove_snapshot if snapshot
        logs.each { |l| l.delete }
      end
    end
    
  end
end