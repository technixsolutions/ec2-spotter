module EC2Spotter
  module Database
    
    module Loggable
      def new_log
        klass = "#{self.class}Log".constantize
        add_log(klass.create)
        log
      end
      
      def log
        log = self.logs_dataset.order(Sequel.desc(:created_at)).first
      end
      
      def clear_logs
        logs.each { |l| l.delete }
      end
    end
    
    module Logger
      def record(event, message, backtrace=nil)
        record_event(event, message, backtrace)
        puts_event(event, message, backtrace)
      end
      
      def record_event(event, message, backtrace=nil)
        self.content = '' if !content
        self.content += "[#{event}] #{message}\n"
        if backtrace
          self.content += "-" * 30
          self.content += backtrace.to_s
          self.content += "-" * 30
          self.content += "\n\n"
        end
        self.save
      end
      
      def puts_event(event, message, backtrace=nil)
        puts "[#{event}] #{message}"
        if backtrace
          puts "-" * 30
          puts backtrace
          puts "-" * 30
          puts
        end
        save
      end
    end
      
  end
end