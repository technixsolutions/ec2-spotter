Backburner.configure do |config|
  config.beanstalk_url    = ["beanstalk://127.0.0.1"]
  config.tube_namespace   = "ec2spotter"
  config.on_error         = lambda { |e| puts e }
  config.max_job_retries  = 3 # default 0 retries
  config.retry_delay      = 10 # default 5 seconds
  config.default_priority = 65536
  config.respond_timeout  = 120
  config.default_worker   = Backburner::Workers::Simple
  config.logger           = Logger.new("/tmp/heartbeat-#{Time.now.strftime("started_%m-%d-%Y_%H-%M-%S")}.log")
  config.priority_labels  = { :asap => 50, :time_insensitive => 500 }
end